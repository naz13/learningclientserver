import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class ExampleClient {
    public static void main(String[] args) {
        String inetAddr = "127.0.0.1";
        int port = 8888;
        OutputStream writer = null;
        BufferedReader reader = null;

        try{
            Socket socket = new Socket (inetAddr,port);
            System.out.println("connected");
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = socket.getOutputStream();
            writer.write(65);
            writer.flush();

            int firstChar = reader.read();
            System.out.println("the first character as integer" +firstChar);
            System.out.println("the first character" + String.valueOf((char)firstChar));
        }catch(Exception e){
            System.out.println("unable to connect");
            System.out.println(e.getMessage());
        }
    }


}
